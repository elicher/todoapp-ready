const mongoose = require('mongoose');
const Schema = mongoose.Schema;
const todosSchema =  new Schema({
    todo: {
		type: String,
        unique: true
	},
    done:Boolean
	})
module.exports =  mongoose.model('todos', todosSchema);