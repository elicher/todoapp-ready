const { db } = require('../models/TodosModel.js');
const Todos = require('../models/TodosModel.js');
const express     = require('express'),
router = express.Router();

class TodosController {

    async findAll(req, res){
        try{
            const foundTodos = await Todos.find();
            res.send(foundTodos);
        }
        catch(e){
            res.send({e})
        }
    }

async insert (req, res) {
    let { todo, done } = req.body;
    console.log(todo , done)
    try{
        const result = await Todos.create({todo, done});
        res.send(result)
    }
    catch(e){
        res.send({e})
    }
}

    async delete (req, res){
        console.log('delete!!!')
        let { todo } = req.body;
        try{
            const removed = await Todos.deleteOne({"todo":todo});
            res.send(removed);
        }
        catch(error){
            res.send({error});
        };
    }

    async update (req, res){
        let { todoname, donestatus } = req.body;
        try{
            const updated = await Todos.updateOne({todo: todoname}, {$set:{done:donestatus}});
            res.send({updated});
        }
        catch(error){
            res.send("error");
        };
    }


};

module.exports = new TodosController();
