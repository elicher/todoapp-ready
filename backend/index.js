const express = require('express'),
    app = express(),
    mongoose = require('mongoose'),
    todosRoute = require('./routes/TodosRoute'),
    bodyParser = require('body-parser');
// =================== initial settings ===================

app.use(bodyParser.urlencoded({extended: true}));
app.use(bodyParser.json());
const cors = require ("cors")
app.use(cors())
// connnect to mongo

// connecting to mongo and checking if DB is running
async function connecting(){
try {
    await mongoose.connect('mongodb+srv://elicher:123456789HELLO@testapp.qtev7.mongodb.net/testapp?retryWrites=true&w=majority', { useUnifiedTopology: true , useNewUrlParser: true })
    console.log('Connected to the DB')
} catch ( error ) {
    console.log('ERROR: Seems like your DB is not running, please start it up');
}
}
connecting()
// end of connecting to mongo and checking if DB is running

// routes
app.use('/todos', todosRoute);
app.use('/users', todosRoute);
app.listen(3016, () => console.log(`listening on port 3016!`))