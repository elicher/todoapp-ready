import '../App.css';
import React from 'react';
import { useRecoilState } from "recoil";
import { todosState } from '../atoms/todosState.js'
import { todoState } from '../atoms/todoState.js'

import '../index.css';
import Form from './Form.js';
import RenderTodos from './RenderTodos.js';

function App() {

  const [todo, setTodo] = useRecoilState(todoState)
    
  const [todos, setTodos] = useRecoilState(todosState)
  
    return (
      <div className="App">
        < Form />
        < RenderTodos />
      </div>
    );
  }
  
  export default App;
  
  