import '../App.css';
import { React, useEffect } from 'react';
import { useRecoilState } from "recoil";
import axios from 'axios';
import { todosState } from '../atoms/todosState.js'
import { todoState } from '../atoms/todoState.js'

import '../index.css';
import App from './App';

function RenderTodos() {

    const [todo, setTodo] = useRecoilState(todoState)

    const [todos, setTodos] = useRecoilState(todosState)

    const deleteTodo = e => {
        e.preventDefault()
        let value = e.target.value
        let newTodos = todos.filter((item) => item.todo !== value);
        setTodos(newTodos)
        console.log(todos)
        console.log(e.target.value)
        axios.post(`http://localhost:3016/todos/delete`, {todo:value})
        .then((res)=>{
          console.log("deleted")
        })
        .catch((err)=>{
          console.log(err)
        })
    

    }

    const updateTodo=(todoname, donestatus)=>{
        axios.post(`http://localhost:3016/todos/update`, (
            { todoname,donestatus }
         ))
        .then((res)=>{
          console.log(res, todoname, donestatus)
        })
        .catch((err)=>{
          console.log(err)
        })
    }
    const strikeThrough = (e,i) => {
        let tempTodos = [...todos]
        let currentTodo={...tempTodos[i]}
        currentTodo.done=!currentTodo.done
        tempTodos[i]=currentTodo
        setTodos(tempTodos)
        console.log("this is currenttodo.todo -> "+currentTodo.todo)
        console.log("this is currenttodo.done -> "+currentTodo.done)
        const a = currentTodo.todo
        const b = currentTodo.done
        updateTodo(a,b)
      }
      
      const style = (item) => {
        var check = (todos.findIndex(el => el.todo === item.todo))
        if (todos[check].done==true) {
          return true
        } else {
          return false
        }
      }

      let getTodos=()=>{
          console.log("axios started")
        axios.get(`http://localhost:3016/todos/`)
        .then((res)=>{
            console.log(res.data)
          setTodos(res.data)
        })
        .catch((err)=>{
          console.log(err)
        })
      }
    
      useEffect(() => {
        getTodos();
      },[]);
    

    const renderList = () => {
        return todos.map((item, index) => {
            return (
                <div key={index} className="row"><h2 value={item.todo} className={((style(item) === true) ? 'strike' : 'normal')}>{item.todo}</h2>
                    <div className="iconwrap"><button className="icons" onClick={deleteTodo} value={item.todo}>🗑</button>
                    <button className="icons" value={item.todo} onClick={(e,i)=>strikeThrough(e,index)}>✅</button></div>
                </div>
            )
        })
    }
    return (
        <div>
            {renderList()}
        </div>
    )
}

export default RenderTodos